#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/poll.h>
#include <ifaddrs.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <errno.h>

#ifndef HAVE_RPRT
#define HAVE_RPRT
#define RPRT_PORT_STRING "5335"
#endif
#include "dnsmasq.h"

const char *prog = NULL;

void print_help(void)
{
    fprintf(stderr, "Usage:\n");
    fprintf(stderr, " %s <actions>\n\n", prog);
    fprintf(stderr, "Actions:\n");
    fprintf(stderr, " -p, --push                        Push local computer [hostname:address] to server.\n");
    fprintf(stderr, " -s, --server <host>               Override default server.\n");
    fprintf(stderr, "     --name <hostname>             Override hostname.\n");
    fprintf(stderr, "     --ack                         Ask server to send an ack after the primary action.\n");
    fprintf(stderr, " -h, --help                        Print this help.\n");
    fprintf(stderr, " -v, --version                     Print version infomation.\n\n");
    fprintf(stderr, "Examples:\n");
    fprintf(stderr, " Override name: %s --name mycomputer --push\n", prog);
    fprintf(stderr, " Override host: %s --server localhost --push\n", prog);
    fprintf(stderr, " Wait for an ack: %s --push -s localhost --ack\n\n", prog);

    exit(1);
}

int checkhdr(struct report* rt) {
    errno = 0;
    if((rt->flags & 0xF0) != RPRT_OKAY) {
        errno = EPROTOTYPE;
        return -1;
    }
    return 0;
}

static int makesocket(const char* host, const char* port, struct sockaddr *sa) {
    struct addrinfo hints;
    struct addrinfo *result, *rp;
    int s, sockfd;

    memset(&hints, 0, sizeof(struct addrinfo));
    #ifdef HAVE_IPV6
    hints.ai_family = AF_UNSPEC;
    #else
    hints.ai_family = AF_INET;
    #endif
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_flags = AI_NUMERICSERV;

    s = getaddrinfo(host, port, &hints, &result);
    if (s != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
        return -1;
    }

    for(rp = result; rp != NULL; rp = rp->ai_next){
        sockfd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        if (sockfd != -1) break;
    }
    if(rp == NULL){
        fprintf(stderr, "makesocket failed.\n");
        return -1;
    }
    if(sa != NULL){
        *sa = *(rp->ai_addr);
    }
    freeaddrinfo(result);
    return sockfd;
}

int main(int argc, char const *argv[])
{
    register char *cp;
    if (argv[0] == NULL) prog = "nsreporter";
    else if ((cp = strrchr(argv[0], '/')) != NULL) prog = cp + 1;
    else prog = argv[0];

    const char *server = "localhost";

    static struct option longopts[] =
    {
        { "help", no_argument, NULL, 'h' },
        { "push", no_argument, NULL, 'p' },
        { "server", required_argument, NULL, 's' },
        { "version", no_argument, NULL, 'v' },
        { "name", required_argument, NULL, 256 },
        { "ack", no_argument, NULL, 257 },
        #ifdef HAVE_IPV6
        { "ipv6", no_argument, NULL, 258 },
        #endif
        { NULL, 0, NULL, 0 }
    };

    static int oc;
    extern int optind;
    extern char *optarg;
    char *hname = NULL;

    (void) setlocale(LC_ALL, "");

    unsigned int flag = 0u;
    flag |= RPRT_PUSH;
    int family = AF_INET;

    while ((oc = getopt_long(argc, (char * const *)argv, "hps:v", longopts, NULL)) != -1)
    {
        switch (oc)
        {
        case 'h':
        {
            print_help();
            break;
        }
        case 'p':
        {
            break;
        }
        case 's':
        {
            server = optarg;
            break;
        }
        case 256:
        {
            hname = optarg;
            break;
        }
        case 257:
        {
            flag |= RPRT_ACKN;
            break;
        }
        case 258:
        {
            family = AF_INET6;
            flag |= RPRT_IPV6;
            break;
        }
        case 'v':
        {
            //break;
        }
        default:
            print_help();
        }
    }

    argc -= optind;
    argv += optind;
    
    if (flag == 0) print_help();

    struct report report;
    memset(&report, 0, sizeof(struct report));
    report.proto = RPRT_PRNO;
    report.flags = flag;
    char buffer[ADDRSTRLEN];

    if (flag & RPRT_PUSH)
    {
        int ret = 0;
        if (hname == NULL)
        {
            char hostname[64];
            ret = gethostname(hostname, 64);
            if (ret == -1)
            {
                perror("gethostname");
                abort();
            }
            // get short version of host name;
            char *ch = strchr(hostname, '.');
            if (ch != NULL)
            {
                *ch = '\0';
            }
            hname = hostname;
        }
        memcpy(report.payload.hostname, hname, strlen(hname));

        int sockfd;
        struct sockaddr_in s;

        sockfd = makesocket(server, RPRT_PORT_STRING, (struct sockaddr *)&s);
        if(sockfd == -1){
            abort();
        }
        socklen_t len = sizeof(s);

        if(sendto(sockfd, &report, sizeof(struct report), 0, (struct sockaddr*)&s, len) == -1){
            perror("sendto");
            abort();
        }

        if (flag & RPRT_ACKN) {
            fprintf(stderr, "receiving ack... ");
            struct pollfd pfd;
            pfd.fd = sockfd;
            pfd.events = POLLIN | POLLPRI;
            int ret = poll(&pfd, 1, 1500);
            if (ret == -1) {
                perror("poll");
                abort();
            }
            if (ret == 0) {
                fprintf(stderr, "timeout.\n");
                abort();
            }
            if(pfd.revents & (POLLIN | POLLPRI)){
                fprintf(stderr, "ok.\n");
                ret = recvfrom(sockfd, &report, sizeof(struct report), 0, (struct sockaddr*)&s, &len);
                if (ret == -1) {
                    perror("recvfrom");
                    abort();
                }
                if (checkhdr(&report) == -1) {
                    perror("checkhdr");
                    abort();
                }

                if(report.flags & RPRT_IPV6)
                {
                    inet_ntop(AF_INET6, &(report.payload.address.v6.sin6_addr), buffer, INET6_ADDRSTRLEN);
                }
                else 
                {
                    inet_ntop(AF_INET, &(report.payload.address.v4.sin_addr), buffer, INET_ADDRSTRLEN);
                }
                fprintf(stderr, "dnsmasq server update cache [%s %s].\n", report.payload.hostname, buffer);
            }
        }
        close(sockfd);
    }

    return 0;
}
