/*  reporter.c is Copyright (c) 2012 Marcus Zy

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 dated June, 1991, or
    (at your option) version 3 dated 29 June, 2007.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "dnsmasq.h"

#ifdef HAVE_RPRT
#include <sys/poll.h>

void reply_ack(int fd, struct report *rt, struct sockaddr *sk)
{
    struct pollfd fds;
    fds.fd = fd;
    fds.events = POLLOUT | POLLWRBAND;
    int ret = poll(&fds, 1, 5);
    if (ret == -1 || ret == 0)
    {
        return;
    }
    if (fds.revents & (POLLOUT | POLLWRBAND))
    {
        if (sendto(fd, rt, sizeof(struct report), 0, sk, INET_ADDRSTRLEN) == -1)
        {
            my_syslog(LOG_ERR, _("reply_ack: error %d occurs."), errno);
            return;
        }
    }
}

void accept_report(struct listener *listen, time_t now)
{
    int fd = listen->rprtfd;
    struct sockaddr_in faddr;
    socklen_t len = sizeof(faddr);
    struct report report;
    memset(&report, 0, sizeof(struct report));
    int ret;
    while ((ret = recvfrom(fd, &report, sizeof(struct report), 0, (struct sockaddr *)&faddr, &len)) == -1 && errno == EAGAIN);
    if (ret == -1)
    {
        my_syslog(LOG_ERR, _("%d: accept_report: unkonw error: %d"), now, errno);
        return;
    }

    if (inet_ntop(faddr.sin_family, &faddr.sin_addr, daemon->addrbuff, ADDRSTRLEN) == NULL)
    {
        my_syslog(LOG_ERR, ("%d: inet_ntop() error. abort cache insertion."), now);
        return;
    }

    if (report.proto != RPRT_PRNO)
    {
        report.flags |= RPRT_ABRT;
    }
    else
    {
        report.flags &= 0x0F;
        report.flags |= RPRT_OKAY;

        /*if (RPRT_PULL & report.flags)
        {
            report_cache(&report);
        }*/

        if (RPRT_PUSH & report.flags)
        {
            report.payload.address.v4 = faddr;
            write_record(&report);
        }
    }

    if (RPRT_ACKN & report.flags)
    {
        reply_ack(fd, &report, (struct sockaddr *)&faddr);
    }

}


#endif
