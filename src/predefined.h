#if __GNUC__ >= 3
#undef inline
#define inline          inline __attribute__ ((always_inline))
#define __Noinline      __attribute__ ((noinline))
#define __Pure          __attribute__ ((pure))
#define __Const         __attribute__ ((const))
#define __Noreturn      __attribute__ ((noreturn))
#define __Malloc        __attribute__ ((malloc))
#define __Must_check    __attribute__ ((warn_unused_result))
#define __Deprecated    __attribute__ ((deprecated))
#define __Used          __attribute__ ((used))
#define __Unused        __attribute__ ((unused))
#define __Packed        __attribute__ ((packed))
#define __Align(x)      __attribute__ ((aligned (x)))
#define __Align_max     __attribute__ ((aligned))
#define Likely(x)       __builtin_expect (!!(x), 1)
#define Unlikely(x)     __builtin_expect (!!(x), 0)
#else
#define __Noinline      /* no noinline */
#define __Pure          /* no pure */
#define __Const         /* no const */
#define __Noreturn      /* no noreturn */
#define __Malloc        /* no malloc */
#define __Must_check    /* no warn_unused_result */
#define __Deprecated    /* no deprecated */
#define __Used          /* no used */
#define __Unused        /* no unused */
#define __Packed        /* no packed */
#define __Align(x)      /* no aligned (x) */
#define __Align_max     /* no aligned */
#define Likely(x)       (x)
#define Unlikely(x)     (x)
#endif